#!/bin/bash

set -e
set -u
set -o pipefail

# Install homebrew
xcode-select --install
/bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install.sh)"
brew --version >/dev/null
if [[ $? -eq 0 ]]; then
    echo "[OK]  Brew has been installed!"
    brew upgrade && brew update
    install_things()
else
    echo "[WARN] Unable to install brew"
    exit 1
fi


install_things() {
# Install GNU utils instead of MacOS equivalents
# Upgrade old tools on Mac
    brew install coreutils \
        findutils \
        diffutils \ 
        gnu-indent \
        gnu-sed \
        ed \ 
        gnu-tar \
        grep \ 
        gnu-which \ 
        gawk \ 
        gzip \
        watch \
        gnutls \
        wget \
        curl \ 
        make \ 
        fd \
        bat \
        insect \
        hyperfine \ 
        neofetch \
        iterm2 \
        spotify \
        slack \
        docker \
        bash \
        less \
        nano \
        python \
        pyenv \
        openssh \
        git \
        vim \
        gpg \
        zsh \
        zsh-completions \
        zsh-autosuggestions \
        zsh-syntax-highlighting

# Zsh and oh-my-zsh
    chsh -s /usr/local/bin/zsh
    sh -c "$(curl -fsSL https://raw.github.com/robbyrussell/oh-my-zsh/master/tools/install.sh)"

# Unversioned symlinks `python`, `python-config`, `pip` etc. pointing to
# `python3`, `python3-config`, `pip3` etc., respectively, have been installed into
#  /usr/local/opt/python/libexec/bin
    pyenv install 3.7.5
    pyenv install 3.8

# Install apps through Cask
    brew tap homebrew/cask-fonts
    brew cask install font-fira-code firefox visual-studio-code
}
