" .vimrc

"""General
set encoding=utf-8 " The encoding displayed
set fileencoding=utf-8 " The encoding written to file
syntax on " Enable syntax highlight
"set ttyfat " Faster redrawing
set lazyredraw " Only redraw when necessary
set cursorline " Find the current line quickly
set ruler
set nocompatible
set showmatch
set splitright
set guicursor=
set laststatus=2
set number
set updatetime=100

" Automatically delete all trailing whitespaces on save
autocmd BufWritePre * %s/\s\+$//e

" Enable autocompletition

"set wildmode=longest.list.full


""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""'

"""Keymappings

"don't use arrowkeys
"noremap <Up> <NOP>
"noremap <Down> <NOP>
"noremap <Left> <NOP>
"noremap <Right> <NOP>
"inoremap <Up> <NOP>
"inoremap <Down> <NOP>
"inoremap <Left> <NOP>
"inoremap <Right> <NOP>

"save with WW
nnoremap WW :w<cr>

"force quit with QQ
nnoremap QQ :q!<cr>

"save and force quit with WQ
nnoremap WQ :wq!<cr>

" use jj as esc
inoremap jj <Esc>
inoremap jk <Esc>

"autoclose tags
inoremap ( ()<Left>
inoremap { {}<Left>
inoremap [ []<Left>
inoremap " ""<Left>


" copy and paste to/from vim and the clipboard
nnoremap <C-y> +y
vnoremap <C-y> +y
nnoremap <C-p> +P
vnoremap <C-p> +P

" the following key binding lets you move lines up and down with Shift+arrow
nnoremap <C-Down> :m+<CR>
nnoremap <C-Up> :m-2<CR>
inoremap <C-Up> <Esc>:m-2<CR>
inoremap <C-Down> <Esc>:m+<CR>
nnoremap <Leader>f :NERDTreeToggle<Enter>
nnoremap <Leader>g :Goyo<CR>

" clipboard
set clipboard^=unnamed,unnamedplus

" the following key binding lets you move lines up and down with Shift+arrow
nnoremap <C-Down> :m+<CR>
nnoremap <C-Up> :m-2<CR>
inoremap <C-Up> <Esc>:m-2<CR>
inoremap <C-Down> <Esc>:m+<CR>

" Open NERDTree automatically when vim starts up
" autocmd vimenter * NERDTree
" let NERDTreeShowHidden=1
map <silent> <C-n> :NERDTreeToggle<CR>

" close NERDTree after a file is opened
let g:NERDTreeQuitOnOpen=1


"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

""" Visual related configs
" 256 colors
set t_Co=256

" show line number and relative number
set number relativenumber

" access system clipboard
set clipboard=xclipboard

" swapfiles location
set backupdir=/tmp//
set directory=/tmp//

" show the status line all the time
set laststatus=2


""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

""" Indentation

" Use spaces instead of tabs
set expandtab

" 1 tab == 4 spaces
set shiftwidth=4
set tabstop=4

" Auto indent, copy the intendation from the previous line when starting a new
" line
set ai

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

""" Utils

":W sudo saves the file (handle the permission-denied error)
command W w !sudo tee % > /dev/null

vmap <C-c> y:call system("xclip -i -selection clipboard", getreg("\""))<CR>:call system("xclip -i", getreg("\""))<CR>


if has('persistent_undo')
    set undodir=$HOME/.vim/undo
    set undolevels=10000
    set undofile
endif

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
""" Plugins management

call plug#begin()

" Dracula theme

Plug 'scrooloose/nerdtree', { 'on': 'NERDTreeToggle' }
Plug 'Valloric/YouCompleteMe', { 'do': './install.py' }
Plug 'vim-airline/vim-airline'
Plug 'vim-airline/vim-airline-themes'
Plug 'junegunn/fzf', { 'dir': '~/.fzf', 'do': './install --all' }
Plug 'junegunn/fzf.vim'
Plug 'vim-syntastic/syntastic'
Plug 'sheerun/vim-polyglot'
Plug 'kristijanhusak/vim-hybrid-material'
Plug 'airblade/vim-gitgutter'


call plug#end()

set background=dark
colorscheme hybrid_reverse

""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

set statusline+=%#warningmsg#
set statusline+=%{SyntasticStatuslineFlag()}
set statusline+=%*

let g:syntastic_always_populate_loc_list = 1
let g:syntastic_auto_loc_list = 1
let g:syntastic_check_on_open = 1
let g:syntastic_check_on_wq = 0
let g:airline#extensions#tabline#enabled = 1
let g:airline_theme='zenburn'
let g:airline_powerline_fonts = 0
let g:airline#extensions#tabline#fnamemod = ':t'
let g:airline#extensions#tabline#tab_nr_type = 1
let g:fzf_tags_command = 'ctags -R --exclude=.git --exclude=docs --exclude=.venv'
let g:syntastic_c_compiler_options = "-std=gnu11 -g -Wall -Wextra -fPIC -I /usr/include -Isrc/ -Isrc/include/"
let g:gitgutter_terminal_reports_focus=0
