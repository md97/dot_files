# If you come from bash you might have to change your $PATH.
# export PATH=$HOME/bin:/usr/local/bin:$PATH

# Path to your oh-my-zsh installation.
export ZSH=$HOME/.oh-my-zsh
export EDITOR='vim'
#export PROMPT_COMMAND=cat $(pwd) > /tmp/whereami


# Set name of the theme to load --- if set to "random", it will
# load a random theme each time oh-my-zsh is loaded, in which case,
# to know which specific one was loaded, run: echo $RANDOM_THEME
# See https://github.com/robbyrussell/oh-my-zsh/wiki/Themes
ZSH_THEME="af-magic"

# use vim key bindings
# bindkey -vi


# Set list of themes to pick from when loading at random
# Setting this variable when ZSH_THEME=random will cause zsh to load
# a theme from this variable instead of looking in ~/.oh-my-zsh/themes/
# If set to an empty array, this variable will have no effect.
# ZSH_THEME_RANDOM_CANDIDATES=( "robbyrussell" "agnoster" )

# Uncomment the following line to use case-sensitive completion.
# CASE_SENSITIVE="true"

# Uncomment the following line to use hyphen-insensitive completion.
# Case-sensitive completion must be off. _ and - will be interchangeable.
# HYPHEN_INSENSITIVE="true"

# Uncomment the following line to disable bi-weekly auto-update checks.
# DISABLE_AUTO_UPDATE="true"

# Uncomment the following line to change how often to auto-update (in days).
 export UPDATE_ZSH_DAYS=13

# Uncomment the following line to disable colors in ls.
# DISABLE_LS_COLORS="true"

# Uncomment the following line to disable auto-setting terminal title.
# DISABLE_AUTO_TITLE="true"

# Uncomment the following line to enable command auto-correction.
 ENABLE_CORRECTION="true"

# Uncomment the following line to display red dots whilst waiting for completion.
 COMPLETION_WAITING_DOTS="true"

# Uncomment the following line if you want to disable marking untracked files
# under VCS as dirty. This makes repository status check for large repositories
# much, much faster.
# DISABLE_UNTRACKED_FILES_DIRTY="true"

# Uncomment the following line if you want to change the command execution time
# stamp shown in the history command output.
# You can set one of the optional three formats:
# "mm/dd/yyyy"|"dd.mm.yyyy"|"yyyy-mm-dd"
# or set a custom format using the strftime function format specifications,
# see 'man strftime' for details.
# HIST_STAMPS="mm/dd/yyyy"

# Would you like to use another custom folder than $ZSH/custom?
# ZSH_CUSTOM=/path/to/new-custom-folder

# Which plugins would you like to load?
# Standard plugins can be found in ~/.oh-my-zsh/plugins/*
# Custom plugins may be added to ~/.oh-my-zsh/custom/plugins/
# Example format: plugins=(rails git textmate ruby lighthouse)
# Add wisely, as too many plugins slow down shell startup.
plugins=(
	git
	z
	zsh-syntax-highlighting
	zsh-autosuggestions
	vi-mode
	colorize
	colored-man-pages
)

source $ZSH/oh-my-zsh.sh

# User configuration

# export MANPATH="/usr/local/man:$MANPATH"

# You may need to manually set your language environment
# export LANG=en_US.UTF-8

# Preferred editor for local and remote sessions
 if [[ -n $SSH_CONNECTION ]]; then
   export EDITOR='vim'
 else
   export EDITOR='vi'
 fi

# Compilation flags
# export ARCHFLAGS="-arch x86_64"

# ssh
export SSH_KEY_PATH="~/.ssh/rsa_id"

# go path
export GOPATH=$HOME/go
export PATH=$PATH:/usr/local/go/bin:$GOPATH/bin

# Set personal aliases, overriding those provided by oh-my-zsh libs,
# plugins, and themes. Aliases can be placed here, though oh-my-zsh
# users are encouraged to define aliases within the ZSH_CUSTOM folder.
# For a full list of active aliases, run `alias`.
#
# Example aliases
alias vimconfig="sudo vim ~/.vimrc"
alias vim vimconfig="sudo vim /.vimrc"
alias :q="exit"
alias :q!="exit"
alias copy="xclip -sel clip"

alias zshconfig="sudo vim ~/.zshrc"
alias vim zshconfig="sudo vim /.zshrc"
# alias ohmyzsh="mate ~/.oh-my-zsh"
alias desktop="ssh mauro@192.168.1.105"
alias rpi="ssh mauro@192.168.1.115"
alias SS='sudo systemctl'
alias g='git'

bindkey -v
bindkey -M viins ‘jj’ vi-cmd-mode
bindkey ‘^R’ history-incremental-search-backward


#[ -f ~/.fzf.zsh ] && source ~/.fzf.zsh

# Functions
#

# # ex - archive extractor
# usage: ex <file>
ex ()
{
  if [ -f $1 ] ; then
    case $1 in
      *.tar.bz2)   tar xjf $1   ;;
      *.tar.gz)    tar xzf $1   ;;
      *.bz2)       bunzip2 $1   ;;
      *.rar)       unrar x $1     ;;
      *.gz)        gunzip $1    ;;
      *.tar)       tar xf $1    ;;
      *.tbz2)      tar xjf $1   ;;
      *.tgz)       tar xzf $1   ;;
      *.zip)       unzip $1     ;;
      *.Z)         uncompress $1;;
      *.7z)        7z x $1      ;;
      *)           echo "'$1' cannot be extracted via ex()" ;;
    esac
  else
    echo "'$1' is not a valid file"
  fi
}

#extract() {
#    local c e i
#
#    (($#)) || return
#
#    for i; do
#        c=''
#        e=1
#
#        if [[ ! -r $i ]]; then
#            echo "$0: file is unreadable: \`$i'" >&2
#            continue
#        fi
#
#        case $i in
#            *.t@(gz|lz|xz|b@(2|z?(2))|a@(z|r?(.@(Z|bz?(2)|gz|lzma|xz)))))
#                   c=(bsdtar xvf);;
#            *.7z)  c=(7z x);;
#            *.Z)   c=(uncompress);;
#            *.bz2) c=(bunzip2);;
#            *.exe) c=(cabextract);;
#            *.gz)  c=(gunzip);;
#            *.rar) c=(unrar x);;
#            *.xz)  c=(unxz);;
#            *.zip) c=(unzip);;
#            *)     echo "$0: unrecognized file extension: \`$i'" >&2
#                   continue;;
#        esac
#
#        command "${c[@]}" "$i"
#        ((e = e || $?))
#    done
#    return "$e"
#}

# Simple function to get the current weather on your cli

weather(){
    if [[ $# -eq 0 ]]; then
        curl http://wttr.in
    else
        curl "http://wttr.in/$1?m"
    fi
}

#Very often changing to a directory is followed by the ls command to list its contents. Therefore it is helpful to have a second function doing both at once. In this example we will name it cl (change list) and show an error message if the specified directory does not exist.

cl() {
	local dir="$1"
	local dir="${dir:=$HOME}"
	if [[ -d "$dir" ]]; then
		cd "$dir" >/dev/null; ls
	else
		echo "bash: cl: $dir: Directory not found"
	fi
}

# mkdir and cd into it

mkcd() {
    mkdir -p "$@" && cd "$@";
}
