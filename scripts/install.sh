#!/bin/bash
#script to set up new pc running ubuntu, with i3 as a WM and MATE as DE
sudo apt-get update -y &&\
sudo apt-get upgrade -y &&\
sudo apt-get install libgdk-pixbuf2.0-0:i386 libgdk-pixbuf2.0-common -y\
&&\
sudo apt-get install -y i3-wm\
    i3lock\
    firefox\
    mpv\
    rofi\
    ranger\
    htop\
    git\
    mhddfs\
    vim\
    curl\
    wget\
    zsh\
    openssh-server\
    openssh-client\
    flameshot\
    tree\
    libconfuse-dev\
    libyajl-dev\
    #libasound2-dev\ revisar
    libiw-dev\
    asciidoc\
    libnl-genl-3-dev\
    syncthing\
    youtube-dl\
    fonts-powerline\
    deluged\
    deluge-web\
    deluge-console\
    nzbdrone\
    tmux\
    docker-ce\
    docker-ce-cli\
    containerd.io\
    xclip\
