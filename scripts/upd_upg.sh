#!/bin/bash

#set -x

sudo apt-get update -y 2>&1 \
&&\
sudo apt-get upgrade -y 2>&1 \
&&\
sudo apt-get autoclean -y 2>&1 \
&&\
sudo apt-get autoremove -y 2>&1 \
&&\
echo "the system has been upgraded, updated, and old packaged has been removed succesfully at $(date)" >> /home/mauro/.cron.log
