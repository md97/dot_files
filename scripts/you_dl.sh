#!/bin/bash
#youtube-dl script for downloading youtube videos's audio with embed thumbail

# uncomment to debug
#set -xeu

#echo "How many videos are you downloading? "
#read NUMBER_OF_FILES


if [ "$(pwd)" != '/media/multimedia/sync' ];
then
    cd /media/multimedia/sync
fi

youtube-dl -o '%(title)s.%(ext)s' -i --no-mtime --mark-watched -R 10 --restrict-filenames --extract-audio --audio-format mp3 --audio-quality 0 --embed-thumbnail \
    https://www.youtube.com/watch?v=UrLwbg17RWA \
    https://www.youtube.com/watch?v=f3muyuJHS0o \
    https://www.youtube.com/watch?v=U4P7ZxItJqE \
    https://www.youtube.com/watch?v=Z7TRzNvWUD4 \
#    >/dev/null 2>&1
#&&\
#DOWNLOADED=$(ls -lrt /media/multimedia/sync | tail -n $NUMBER_OF_FILES) &&\
#echo $(date) $DOWNLOADED >> /media/multimedia/you_dl.log
